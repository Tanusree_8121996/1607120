<?php

   session_start();

?>

<!DOCTYPE html>
<html>
<head>
<style>
#doctors {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#doctors td, #doctors th {
  border: 1px solid #ddd;
  padding: 8px;
}

#doctors tr:nth-child(even){background-color: #f2f2f2;}

#doctors tr:hover {background-color: #ddd;}

#doctors th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: brown;
  color: white;
}
</style>
<title>Find doctors</title>
</head>
<body>

<table id="doctors">
  <tr>
    <th>Name</th>
    <th>Country</th>
    <th>phone number</th>
    <th>Room number</th>
  </tr>
  <tr>

    <td>Dr.Shapan Islam</td>
    <td>Bangladesh</td>
    <td>015432478786</td>
    <td>209</td>
  </tr>
  <tr>

    <td>Dr.Basudeb Dutta</td>
    <td>Bangladesh</td>
    <td>017313668780</td>
    <td>402</td>
    
  </tr>

  <tr>
    
    <td>Dr.Chang Rovelli</td>
    <td>China</td>
    <td>066534323214</td>
    <td>405</td>
  </tr>
  <tr>

    <td>Dr.Helena Besto</td>
    <td>UK</td>
    <td>053465678787</td>
    <td>417</td>
  </tr>

  <tr>
    
    <td>Dr.Adiba Islam</td>
    <td>Bangladesh</td>
    <td>0187565334236</td>
    <td>411</td>
  </tr>
  
  
  
</table>
<br><br>
<?php

   echo $_SESSION ['User'];

?>
</body>
</html>
