<?php
     session_start();
?>


<!DOCTYPE html>
<html>
<head>
    <title>Patient Login</title>
    
    <link rel="stylesheet" type= "text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <script>
function validateForm() 
{
  var x = document.forms["myForm"]["user"].value;
  if (x == "") {
    alert("Name must be filled out");
    return false;
  }

}
</script>

</head>
<style>
    body{
    background-size:cover;
    background-position: center;
    background-color: palegreen;
}

.login-box{
    max-width: 900px;
    float: none;
    margin: 150px auto;
}

.login-left{
    background:skyblue;
    padding: 30px;

}

.form-control{
    background-color: transparent !important;
}
</style>
<body>
<h1 style="color:blue">You need to login by your name and password</h1>
 <div class="container">
   <div class="login-box">
    <div class="row">
        <div class="col-md-6 login-left">
        <h2> Login here </h2>
        <form name="myForm" action="validation.php" onsubmit="return validateForm()" method="post">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="user" class="form-control">

            </div>
            <br>

            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
                
            </div>
            <br><br>
            <button type="submit" style="font-size:150%; color:black;" class="w3-button w3-border w3-hover-blue"> Log in</button>
        </form>
        </div>

        

    </div>

    </div>
 
 </div>
</body>
<?php

   echo $_SESSION ['User'];

?>
</html>