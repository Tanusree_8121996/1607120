<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "tithy";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}

// sql to create table
$sql = "CREATE TABLE patient (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(30) NOT NULL,
email VARCHAR(50),
mobile VARCHAR(50),
doctor VARCHAR(50),
password VARCHAR(50), 
reg_date TIMESTAMP
)";

if (mysqli_query($conn, $sql)) {
    echo "Table created successfully";
} else {
    //echo "Error creating table: " . mysqli_error($conn);
}

mysqli_close($conn);
?>