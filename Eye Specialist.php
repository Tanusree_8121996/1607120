<?php

   session_start();

?>
<!DOCTYPE html>
<html>
<head>
<style>
#doctors {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#doctors td, #doctors th {
  border: 1px solid #ddd;
  padding: 8px;
}

#doctors tr:nth-child(even){background-color: #f2f2f2;}

#doctors tr:hover {background-color: #ddd;}

#doctors th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: brown;
  color: white;
}
</style>
<title>Find doctors</title>
</head>
<body>

<table id="doctors">
  <tr>
    <th>Name</th>
    <th>Country</th>
    <th>phone number</th>
    <th>Room number</th>
  </tr>
  <tr>

    <td>Ashok Roy</td>
    <td>India</td>
    <td>095432478786</td>
    <td>205</td>
  </tr>

  <tr>
    
    <td>Kiron Chowdhury</td>
    <td>India</td>
    <td>0966534323214</td>
    <td>209</td>
  </tr>
  <tr>

    <td>Maria Selve</td>
    <td>France</td>
    <td>084333668780</td>
    <td>207</td>
    
  </tr>

 
  <tr>

    <td>Michale Francis</td>
    <td>Canada</td>
    <td>089465678787</td>
    <td>217</td>
  </tr>

  <tr>
    
    <td>Michale John</td>
    <td>France</td>
    <td>087565334236</td>
    <td>211</td>
  </tr>
  <tr>
    <td>Sunita Agarkar</td>
    <td>India</td>
    <td>091435421266</td>
    <td>213</td>
  </tr>
  <tr>
    <td>Tasnim Islam</td>
    <td>Bangladesh</td>
    <td>01576877872</td>
    <td>215</td>
  </tr>
  
</table>
<?php

   echo $_SESSION ['User'];

?>
</body>
</html>
