<?php

   session_start();

?>

<!DOCTYPE html>
<html>
<head>
<style>
#doctors {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#doctors td, #doctors th {
  border: 1px solid #ddd;
  padding: 8px;
}

#doctors tr:nth-child(even){background-color: #f2f2f2;}

#doctors tr:hover {background-color: #ddd;}

#doctors th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: brown;
  color: white;
}
</style>
<title>Find doctors</title>
</head>
<body>

<table id="doctors">
  <tr>
    <th>Name</th>
    <th>Country</th>
    <th>phone number</th>
    <th>Room number</th>
  </tr>
  <tr>

    <td>Dr.Christina Berglund</td>
    <td>Sweden</td>
    <td>065432478786</td>
    <td>305</td>
  </tr>
  <tr>

    <td>Dr.Francisco Chang</td>
    <td>Mexico</td>
    <td>084313668780</td>
    <td>307</td>
    
  </tr>

  <tr>
    
    <td>Dr.Giovanni Rovelli</td>
    <td>Italy</td>
    <td>066534323214</td>
    <td>309</td>
  </tr>
  <tr>

    <td>Dr.Helen Bennett</td>
    <td>UK</td>
    <td>053465678787</td>
    <td>317</td>
  </tr>

  <tr>
    
    <td>Dr.Marie Bertrand</td>
    <td>France</td>
    <td>87565334236</td>
    <td>311</td>
  </tr>
  <tr>
    <td>Dr.Maria Anders</td>
    <td>Germany</td>
    <td>014435421266</td>
    <td>313</td>
  </tr>
  <tr>
    <td>Dr.Philip Cramer</td>
    <td>Germany</td>
    <td>04576877872</td>
    <td>315</td>
  </tr>
  
 
  
  
  
</table>
<br><br>
<?php

   echo $_SESSION ['User'];

?>
</body>
</html>
