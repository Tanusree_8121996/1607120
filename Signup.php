<?php
     session_start();
?>

<!DOCTYPE html>
<html>
<head>
    <title>Patient registration</title>
   
    <link rel="stylesheet" type= "text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<script>
function validateForm() 
{
  var x = document.forms["myForm"]["user"].value;
  if (x == "") {
    alert("Name must be filled out");
    return false;
  }

}
</script>


</head>
<style>
    body{
    background-size:cover;
    background-position: center;
    background-color: palegreen;
}

.login-box{
    max-width: 900px;
    float: none;
    margin: 150px auto;
}

.login-right{
    background:skyblue;
    padding: 30px;
}

.form-control{
    background-color: transparent !important;
}
</style>
<body>
 <div class="container">
   <div class="login-box">
    <div class="row">
        <div class="col-md-6 login-right">
        <h2> Register Here to take Appointment </h2>
        <form name="myForm" action="registration.php" onsubmit="return validateForm()" method="post">
            <div class="form-group">
                <label>Username</label>
                <input type="text" name="user" class="form-control">

            </div>
            <br>

            <div class="form-group">
                <label>email</label>
                <input type="text" name="email" class="form-control">

            </div>
            <br>

            <div class="form-group">
                <label>Mobile Number</label>
                <input type="text" name="mobile" class="form-control">

            </div>
            <br>

            <div class="form-group">
                <label>Doctor's name</label>
                <input type="text" name="doctor" class="form-control">

            </div>
            <br>

            <div class="form-group">
                <label>Password</label>
                <input type="password" name="password" class="form-control">
                
            </div>
            <br>

                <button type="submit" style="font-size:150%; color:black;" class="w3-button w3-border w3-hover-blue"> Register </button>
        </form>
        </div>

    </div>

    </div>
 
 </div>
 <?php

   echo $_SESSION ['User'];

?>

</body>
</html>