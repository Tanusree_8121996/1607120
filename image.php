<?php

   session_start();

?>

<!DOCTYPE html>
<html>
<head>
<style>
div.gallery {
  border: 1px solid #ccc;
}

div.gallery:hover {
  border: 1px solid #777;
}

div.gallery img {
  width: 70%;
  height: auto;
}

div.desc {
  padding: 15px;
  text-align: center;
}

* {
  box-sizing: border-box;
}

.responsive {
  padding: 0 6px;
  float: left;
  width: 24.99999%;
}

@media only screen and (max-width: 700px) {
  .responsive {
    width: 49.99999%;
    margin: 6px 0;
  }
}

@media only screen and (max-width: 500px) {
  .responsive {
    width: 100%;
  }
}

.clearfix:after {
  content: "";
  display: table;
  clear: both;
}
</style>
</head>
<body>

<h2>Image Gallery Of Anondomoyi Hospital</h2>
<h3>You can get a clear idea about our hospital seeing this images.</h3>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_5terre.jpg">
      <img src="building.jpg" alt="Cinque Terre" width="500" height="300">
    </a>
    <div class="desc">Main Hospital Building</div>
  </div>
</div>


<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_forest.jpg">
      <img src="download.jpg" alt="Forest" width="500" height="300">
    </a>
    <div class="desc">Emergency Section</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_lights.jpg">
      <img src="download -1.jpg" alt="Northern Lights" width="500" height="300">
    </a>
    <div class="desc">Ambulance</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="download-2.jpg" alt="Mountains" width="500" height="300">
    </a>
    <div class="desc">Operation Theatre</div>
  </div>
</div>
<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="download-3.jpg" alt="Mountains" width="500" height="300">
    </a>
    <div class="desc">Bypass Surgery</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="download-4.jpg" alt="Mountains" width="500" height="300">
    </a>
    <div class="desc">Modern Services</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="download-5.jpg" alt="Mountains" width="500" height="300">
    </a>
    <div class="desc">Hospital Corridore</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="images.jpg" alt="Mountains" width="500" height="300">
    </a>
    <div class="desc">Cabin</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="images-1.jpg" alt="Mountains" width="500" height="300">
    </a>
    <div class="desc">Our Special Doctors</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="images-2.jpg" alt="Mountains" width="500" height="300">
    </a>
    <div class="desc">Mordern Equipments</div>
  </div>
</div>

<div class="responsive">
  <div class="gallery">
    <a target="_blank" href="img_mountains.jpg">
      <img src="images-3.jpg" alt="Mountains" width="500" height="300">
    </a>
    <div class="desc">Our Doctors</div>
  </div>
</div>


<div class="clearfix"></div>

<div style="padding:6px;">

</div>
<br><br>
<?php

   echo $_SESSION ['User'];

?>
</body>
</html>
