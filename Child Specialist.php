<?php

   session_start();

?>

<!DOCTYPE html>
<html>
<head>
<style>
#doctors {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#doctors td, #doctors th {
  border: 1px solid #ddd;
  padding: 8px;
}

#doctors tr:nth-child(even){background-color: #f2f2f2;}

#doctors tr:hover {background-color: #ddd;}

#doctors th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: brown;
  color: white;
}
</style>
<title>Find doctors</title>
</head>
<body>

<table id="doctors">
  <tr>
    <th>Name</th>
    <th>Country</th>
    <th>phone number</th>
    <th>Room number</th>
  </tr>
  <tr>

    <td>Dr. Joyanta Chatterjee</td>
    <td>India</td>
    <td>95432478786</td>
    <td>106</td>
  </tr>
  <tr>

    <td>Dr. Mave Francisco</td>
    <td>Mexico</td>
    <td>084313668780</td>
    <td>208</td>
    
  </tr>

  <tr>
    
    <td>Dr. Adriza Bhattacharjee</td>
    <td>India</td>
    <td>96534323214</td>
    <td>211</td>
  </tr>
  <tr>

    <td>Dr. Taslima alam</td>
    <td>Bangladesh</td>
    <td>013465678787</td>
    <td>118</td>
  </tr>

  <tr>
    
    <td>Dr. Nina Francis</td>
    <td>France</td>
    <td>87565334236</td>
    <td>214</td>
  </tr>
  <tr>
    <td>Dr.Maria Moumi</td>
    <td>Germany</td>
    <td>04435421266</td>
    <td>316</td>
  </tr>
  <tr>
    <td>Dr. Ashok Kumar</td>
    <td>India</td>
    <td>9876877872</td>
    <td>125</td>
  </tr>
  
  
  
  
  
</table>
<br><br>
<?php

   echo $_SESSION ['User'];

?>
</body>
</html>
