<?php

   session_start();

?>

<!DOCTYPE html>
<html>
<head>
<style>
#doctors {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#doctors td, #doctors th {
  border: 1px solid #ddd;
  padding: 8px;
}

#doctors tr:nth-child(even){background-color: #f2f2f2;}

#doctors tr:hover {background-color: #ddd;}

#doctors th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: brown;
  color: white;
}
</style>
<title>Find doctors</title>
</head>
<body>

<table id="doctors">
  <tr>
    <th>Name</th>
    <th>Country</th>
    <th>phone number</th>
    <th>Room number</th>
  </tr>
  <tr>

    <td>Dr.Sanjay Paul</td>
    <td>Bangladesh</td>
    <td>015432478786</td>
    <td>306</td>
  </tr>
  <tr>

    <td>Dr.El Francisco</td>
    <td>Mexico</td>
    <td>084313668780</td>
    <td>308</td>
    
  </tr>

  <tr>
    
    <td>Dr. Abritti Basu</td>
    <td>Bangladesh</td>
    <td>016534323214</td>
    <td>119</td>
  </tr>
  <tr>

    <td>Dr.Yang Ching Ja</td>
    <td>China</td>
    <td>053465678787</td>
    <td>417</td>
  </tr>

  <tr>
    
    <td>Dr.Mariline Bertrand</td>
    <td>France</td>
    <td>87565334236</td>
    <td>411</td>
  </tr>
  <tr>
    <td>Dr.Nira Tratina</td>
    <td>Germany</td>
    <td>014435421266</td>
    <td>212</td>
  </tr>
  <tr>
    <td>Dr.Soummojit Banerjee</td>
    <td>India</td>
    <td>9576877872</td>
    <td>415</td>
  </tr>
  
 
  
  
  
</table>

<?php

   echo $_SESSION ['User'];

?>
</body>
</html>
