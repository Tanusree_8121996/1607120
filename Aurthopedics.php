<?php

   session_start();

?>


<!DOCTYPE html>
<html>
<head>
<style>
#doctors {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#doctors td, #doctors th {
  border: 1px solid #ddd;
  padding: 8px;
}

#doctors tr:nth-child(even){background-color: #f2f2f2;}

#doctors tr:hover {background-color: #ddd;}

#doctors th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: brown;
  color: white;
}
</style>
<title>Find doctors</title>
</head>
<body>

<table id="doctors">
  <tr>
    <th>Name</th>
    <th>Country</th>
    <th>phone number</th>
    <th>Room number</th>
  </tr>
  <tr>

    <td>Dr.Monjurul Alam</td>
    <td>Bangladesh</td>
    <td>0165432478786</td>
    <td>106</td>
  </tr>
  <tr>

    <td>Dr.Hans Hesel</td>
    <td>Sweden</td>
    <td>084313668780</td>
    <td>106</td>
    
  </tr>

  <tr>
    
    <td>Dr.Balle Bassle</td>
    <td>Italy</td>
    <td>066534323214</td>
    <td>108</td>
  </tr>
  <tr>

    <td>Dr.Garie Bennett</td>
    <td>UK</td>
    <td>053465678787</td>
    <td>118</td>
  </tr>

  <tr>
    
    <td>Dr.Shephard Bertrand</td>
    <td>France</td>
    <td>87565334236</td>
    <td>211</td>
  </tr> 
  
  
</table>

<?php

   echo $_SESSION ['User'];

?>
</body>
</html>
