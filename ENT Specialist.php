<?php

   session_start();

?>

<!DOCTYPE html>
<html>
<head>
<style>
#doctors {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#doctors td, #doctors th {
  border: 1px solid #ddd;
  padding: 8px;
}

#doctors tr:nth-child(even){background-color: #f2f2f2;}

#doctors tr:hover {background-color: #ddd;}

#doctors th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color: brown;
  color: white;
}
</style>
<title>Find doctors</title>
</head>
<body>

<table id="doctors">
  <tr>
    <th>Name</th>
    <th>Country</th>
    <th>phone number</th>
    <th>Room number</th>
  </tr>
 
  <tr>

   <td>Dr. Apu Chowdhury</td>
    <td>Bangladesh</td>
    <td>013465678787</td>
    <td>117</td>
  </tr>

  <tr>
    
    <td>Dr. Mike Jobs</td>
    <td>UK</td>
    <td>87565334236</td>
    <td>111</td>
  </tr>
  <tr>
    <td>Dr.Nilima Roy</td>
    <td>India</td>
    <td>94435421266</td>
    <td>113</td>
  </tr>
  <tr>
    <td>Dr.Philip Cramer</td>
    <td>Germany</td>
    <td>04576877872</td>
    <td>315</td>
  </tr>
  
  <tr>
    <td>Dr.Roland Mendel</td>
    <td>Austria</td>
    <td>0243435657676</td>
    <td>316</td>
  </tr>
  
  <tr>
    
    <td>Dr.Simon Crowther</td>
    <td>UK</td>
    <td>024354465755</td>
    <td>319</td>

  </tr>
  <tr>
    
    <td>Dr.Yoshi Tannamuri</td>
    <td>Canada</td>
    <td>04465767443</td>
    <td>310</td>
  </tr>
  
  
  
</table>
<?php

   echo $_SESSION ['User'];

?>
</body>
</html>
